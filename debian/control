Source: vim-snipmate
Section: editors
Priority: optional
Maintainer: Andrea Capriotti <capriott@debian.org>
Build-Depends: debhelper-compat (= 13), dh-sequence-vim-addon
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://github.com/msanders/snipmate.vim
Vcs-Git: https://salsa.debian.org/debian/vim-snipmate.git
Vcs-Browser: https://salsa.debian.org/debian/vim-snipmate

Package: vim-snipmate
Architecture: all
Depends: vim-addon-mw-utils, vim-tlib, ${misc:Depends}, ${vim-addon:Depends}
Suggests: vim-snippets
Description: Vim script that implements some of TextMate's snippets features.
 SnipMate.vim aims to be an unobtrusive, concise vim script that implements
 some of TextMate's snippets features in Vim. A snippet is a piece of
 often-typed text that you can insert into your document using a trigger word
 followed by a <tab>.
 .
 For instance, in a C file using the default installation of snipMate.vim, if
 you type "for<tab>" in insert mode, it will expand a typical for loop in C:
 .
 for (i = 0; i < count; i++) {
 .
 }
 .
 To go to the next item in the loop, simply <tab> over to it; if there is
 repeated code, such as the "i" variable in this example, you can simply start
 typing once it's highlighted and all the matches specified in the snippet will
 be updated.
 .
 snipMate.vim has the following features among others:
 .
  - The syntax of snippets is very similar to TextMate's, allowing easy
    conversion.
  - The position of the snippet is kept transparently (i.e., it does not use
    marks/placeholders inserted into the buffer), allowing you to escape out
    of an incomplete snippet, something particularly useful in Vim.
  - Variables in snippets are updated as-you-type.
  - Snippets can have multiple matches.
  - Snippets can be out of order. For instance, in a do...while loop, the
    condition can be added before the code.
